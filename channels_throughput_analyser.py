import numpy as np
import matplotlib.pyplot as plt
import os
from model.fitsVNAS21Sweep import fitsVNAS21Sweep


def main():
    directory = r"data"

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = directory + "\\" + filename

        if "CH1" in filename:
            if "in_in" in filename:
                ch1_input_sweep = fitsVNAS21Sweep(file_path=file_path)
            if "in_out" in filename:
                ch1_full_through_sweep = fitsVNAS21Sweep(file_path=file_path)
        if "CH3" in filename:
            if "in_in" in filename:
                ch3_input_sweep = fitsVNAS21Sweep(file_path=file_path)
            if "in_out" in filename:
                ch3_full_through_sweep = fitsVNAS21Sweep(file_path=file_path)
        if "CH1" in filename:
            if "in_in" in filename:
                ch4_input_sweep = fitsVNAS21Sweep(file_path=file_path)
            if "in_out" in filename:
                ch4_full_through_sweep = fitsVNAS21Sweep(file_path=file_path)
        if "cable" in filename:
            cable_only_sweep = fitsVNAS21Sweep(file_path=file_path)

    # Subtract cable loss and input attenuation from the full readout line loop
    ch1_corrected_input_line_s21_db = (
        ch1_input_sweep.s21_magnitude_db_array
        - cable_only_sweep.s21_magnitude_db_array
    )
    ch1_corrected_output_line_s21_mag_db = (
        ch1_full_through_sweep.s21_magnitude_db_array
        - ch1_input_sweep.s21_magnitude_db_array
        - cable_only_sweep.s21_magnitude_db_array
    )

    ch3_corrected_input_line_s21_db = (
        ch3_input_sweep.s21_magnitude_db_array
        - cable_only_sweep.s21_magnitude_db_array
    )
    ch3_corrected_output_line_s21_mag_db = (
        ch3_full_through_sweep.s21_magnitude_db_array
        - ch3_input_sweep.s21_magnitude_db_array
        - cable_only_sweep.s21_magnitude_db_array
    )

    ch4_corrected_input_line_s21_db = (
        ch4_input_sweep.s21_magnitude_db_array
        - cable_only_sweep.s21_magnitude_db_array
    )
    ch4_corrected_output_line_s21_mag_db = (
        ch4_full_through_sweep.s21_magnitude_db_array
        - ch4_input_sweep.s21_magnitude_db_array
        - cable_only_sweep.s21_magnitude_db_array
    )

    plt.figure(figsize=(8, 6), num=0)
    plt.plot(
        ch1_full_through_sweep.frequency_array,
        ch1_corrected_output_line_s21_mag_db,
        label="Output line"
    )
    plt.plot(
        ch1_input_sweep.frequency_array,
        ch1_input_sweep.s21_magnitude_db_array,
        label="Input line"
    )
    plt.plot(
        cable_only_sweep.frequency_array,
        cable_only_sweep.s21_magnitude_db_array,
        label="Cable"
    )
    plt.plot(
        ch1_full_through_sweep.frequency_array,
        ch1_full_through_sweep.s21_magnitude_db_array,
        label="Full readout"
    )
    plt.title("Channel 1")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.legend()
    plt.savefig("Channel_1_insertion_loss.jpg", num=0)
    plt.show()


    plt.figure(figsize=(8, 6), num=1)
    plt.plot(
        ch3_full_through_sweep.frequency_array,
        ch3_corrected_output_line_s21_mag_db,
        label="Output line"
    )
    plt.plot(
        ch3_input_sweep.frequency_array,
        ch3_input_sweep.s21_magnitude_db_array,
        label="Input line"
    )
    plt.plot(
        cable_only_sweep.frequency_array,
        cable_only_sweep.s21_magnitude_db_array,
        label="Cable"
    )
    plt.plot(
        ch3_full_through_sweep.frequency_array,
        ch3_full_through_sweep.s21_magnitude_db_array,
        label="Full readout"
    )
    plt.title("Channel 3")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.legend()
    plt.savefig("Channel_3_insertion_loss.jpg", num=1)
    plt.show()

    plt.figure(figsize=(8, 6), num=2)
    plt.plot(
        ch4_full_through_sweep.frequency_array,
        ch4_corrected_output_line_s21_mag_db,
        label="Output line"
    )
    plt.plot(
        ch4_input_sweep.frequency_array,
        ch4_input_sweep.s21_magnitude_db_array,
        label="Input line"
    )
    plt.plot(
        cable_only_sweep.frequency_array,
        cable_only_sweep.s21_magnitude_db_array,
        label="Cable"
    )
    plt.plot(
        ch4_full_through_sweep.frequency_array,
        ch4_full_through_sweep.s21_magnitude_db_array,
        label="Full readout"
    )
    plt.title("Channel 4")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.legend()
    plt.savefig("Channel_4_insertion_loss.jpg", num=2)
    plt.show()


    # Save data to files:
    with open('aloysius_channel_1_cable_corrected_input.txt', 'w') as f:
        f.write("Frequency (Hz) , S21 Magnitude (dB)\n")
        for count, value in enumerate(ch1_input_sweep.frequency_array):
            f.write(str(value) + " , " + str(ch1_corrected_input_line_s21_db[count]) + "\n")

    with open('aloysius_channel_3_cable_corrected_input.txt', 'w') as f:
        f.write("Frequency (Hz) , S21 Magnitude (dB)\n")
        for count, value in enumerate(ch3_input_sweep.frequency_array):
            f.write(str(value) + " , " + str(ch3_corrected_input_line_s21_db[count]) + "\n")

    with open('aloysius_channel_4_cable_corrected_input.txt', 'w') as f:
        f.write("Frequency (Hz) , S21 Magnitude (dB)\n")
        for count, value in enumerate(ch4_input_sweep.frequency_array):
            f.write(str(value) + " , " + str(ch4_corrected_input_line_s21_db[count]) + "\n")

    with open('aloysius_channel_1_cable_corrected_output.txt', 'w') as f:
        f.write("Frequency (Hz) , S21 Magnitude (dB)\n")
        for count, value in enumerate(ch1_full_through_sweep.frequency_array):
            f.write(str(value) + " , " + str(ch1_corrected_output_line_s21_mag_db[count]) + "\n")

    with open('aloysius_channel_3_cable_corrected_output.txt', 'w') as f:
        f.write("Frequency (Hz) , S21 Magnitude (dB)\n")
        for count, value in enumerate(ch3_full_through_sweep.frequency_array):
            f.write(str(value) + " , " + str(ch3_corrected_output_line_s21_mag_db[count]) + "\n")

    with open('aloysius_channel_4_cable_corrected_output.txt', 'w') as f:
        f.write("Frequency (Hz) , S21 Magnitude (dB)\n")
        for count, value in enumerate(ch4_full_through_sweep.frequency_array):
            f.write(str(value) + " , " + str(ch4_corrected_output_line_s21_mag_db[count]) + "\n")


if __name__ == "__main__":
    main()
