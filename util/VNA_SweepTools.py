import numpy as np
import matplotlib.pyplot as plt
from lmfit import Model
from lmfit import Parameters


def solveForY(y0: np.ndarray, a: float, upSweep=True) -> np.ndarray:
    """
    Function to return an array of the correct generator detuning in linewidths
    by solving the cubic function, equation 13 in arXiv 1305.4281. Each value
    in the returned array corresponds to the solution for each y0 value.

    NB: If VNA sweep is carried out with a downwards sweep, upSweep must be
    changed to False.

    """

    solvedYArray = np.empty(shape=y0.shape)

    for count, value in enumerate(y0):
        # Define Cubic Equation:
        cubic = np.poly1d([4.0, -4.0 * value, 1.0, -(value + a)])
        # Solve for roots:
        roots = cubic.r
        # Discard imaginary roots:
        realRoots = []
        for solution in roots:
            if solution.imag == 0:
                realRoots.append(solution.real)

        realRoots = np.array(realRoots)  # Make np array.
        # If VNA sweep is upwards select the minimum valued root, else the max.
        if upSweep == True:
            solvedYArray[count] = np.amin(realRoots)
        else:
            solvedYArray[count] = np.amax(realRoots)

    return solvedYArray


def s21NonLinear(f, f0, Qr, Qcre, Qcim, a, A):
    """
    Function to return the S21 spectrum for frequency array f for a KID with a
    resonant frequency f0, resonator quality factor Qr, real and imaginary
    coupling quality facors Qcre and Qcim, bifurcation factor a and amplitude A.
    """

    from math import pi

    y0 = Qr * (f - f0) / f0
    y = solveForY(y0, a)

    return abs(A * (1 - Qr / (Qcre + 1j * Qcim) / (1 + 2j * y)))


def fitNonLinearMagS21(f, s21, approxQr=1e5, approxa=0.7, dB=False, make_plot=False):
    """
    Function to fit to a single resonance feature of a KID using lmfit.
    Returns the best fit variables as an array of form: np.array([f0, Qr, Qc, Qi, a]),
    followed by the chi sqaured for the fit and a plot of the fit if specified.
    """
    from matplotlib.pyplot import subplots

    # Define model:
    s21Model = Model(s21NonLinear)

    # Assuming minimum value of s21 is f0, hence approx f0.
    minimumIndex = np.argmin(s21)
    approxF0 = f[minimumIndex]

    # Create an array to be used to weight the fit to give higher importance to date around
    # the resonance.
    weights = np.ones(s21.shape)
    weights[minimumIndex - 250 : minimumIndex + 250] = 5

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=approxF0, vary=True)
    params.add("Qr", value=approxQr / 2, min=0.0, vary=True)
    params.add(
        "Qcre", value=approxQr, min=0.0, vary=True
    )  # NB: Qr should be less than Qc
    params.add("Qcim", value=100.0, min=0.0, vary=True)
    params.add("a", value=approxa, vary=True, min=0)
    params.add("A", value=abs(s21).mean())

    # Fit and extract best values and fitting statistics
    result = s21Model.fit(s21, params, f=f, weights=weights)
    bestFit = result.best_fit
    bestValues = result.best_values
    chiSquared = result.chisqr

    f0 = bestValues["f0"]
    Qr = bestValues["Qr"]
    Qcre = bestValues["Qcre"]
    Qcim = bestValues["Qcim"]
    a = bestValues["a"]

    Qe = Qcre + 1j * Qcim
    Qc = abs(Qe) ** 2 / Qe.real
    Qi = 1.0 / (1 / Qr - 1 / Qc)

    # Plot formation
    if make_plot == True:
        fig, ax = plt.subplots(1, 1, num=1)
        # If to convert s21 to dB:
        if dB == True:
            bestFit = 0 * np.log10(bestFit)
            s21 = 20 * np.log10(abs(s21))
        ax.plot(f * 1e-9, s21, linestyle="none", marker=".", color="b", label="Data")
        ax.plot(
            f * 1e-9,
            bestFit,
            linestyle="--",
            color="r",
            label="Best Fit:\n$\chi^2$ = %.2e\nQr = %.2e\nQc = %.2e\nQi = %.2e\na = %.3f"
            % (chiSquared, Qr, Qc, Qi, a),
        )
        ax.set_xlabel("Frequency (GHz)")

        if dB == True:
            ax.set_ylabel("S21 Magnitude (dB)")
        else:
            ax.set_ylabel("S21 Magnitude")

        ax.legend()
        ax.grid()
        return np.array([f0, Qr, Qc, Qi, a]), chiSquared, fig
    else:
        return np.array([f0, Qr, Qc, Qi, a]), chiSquared


def findCollidingKIDS(f0Array: np.ndarray):
    """
    Function to search an array of frequency values and identify
    values that are too similar to their neighbour.
    Returns a nested list of the indices corresponding to the
    colliding values.
    """
    collidingKIDS = []
    for count, value in enumerate(f0Array):
        if count < f0Array.size - 1:
            difference = abs(f0Array[count + 1] - f0Array[count])
            mean = (f0Array[count] + f0Array[count + 1]) / 2
            ratio = difference / mean
            if ratio < 1e-4:
                collidingKIDS.append([count, count + 1])
    return collidingKIDS
