import numpy as np
from dataclasses import dataclass
from astropy.io import fits


class fitsVNAS21Sweep:

    """
    Class to represent the fits data from a segmented VNA sweep of a KID feedline. This is for fits files that have been
    processed by the automat.py script.
    NB: The sweep information must be stored in a BinTableHDU called VNASWEEP
    """

    def __init__(self, file_path: str):
        """
        Function to initiate the VNA sweep data class and extract the relevant values from the data file.
        """

        self.file_path = file_path

        with fits.open(self.file_path) as hduList:
            sweep_params = repr(hduList["VNASWEEP"].header)
            header_vna = hduList["VNASWEEP"].header
            data = hduList["VNASWEEP"].data
            frequency_array = np.empty(shape=len(data))
            real_s21_array = np.empty(shape=len(data))
            imag_s21_array = np.empty(shape=len(data))

            for count, entry in enumerate(data):
                frequency_array[count] = entry[0]
                real_s21_array[count] = entry[1]
                imag_s21_array[count] = entry[2]

            print(sweep_params)
            self.sweep_params = sweep_params
            self.raw_data = data
            self.DUT = header_vna["DUT"]
            self.sample_temperature = header_vna["SAMPLETE"]
            self.attenuation = float(header_vna["INPUTATT"]) + float(
                header_vna["ATT_RT"]
            )
            self.frequency_array = frequency_array
            self.real_s21_array = real_s21_array
            self.imag_s21_array = imag_s21_array
            self.s21_magnitude_array = np.sqrt(
                real_s21_array**2 + imag_s21_array**2
            )
            self.s21_magnitude_db_array = 20 * np.log10(self.s21_magnitude_array)
