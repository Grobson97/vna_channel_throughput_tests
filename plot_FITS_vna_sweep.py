import matplotlib.pyplot as plt
from model.fitsVNAS21Sweep import fitsVNAS21Sweep


def main():
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Aloysius Tests\Feed Through Channel Tests\Aloysius_channel_1\20220803__Data_Prelim\VNA_Sweeps"
    filename = "S21_Continuous_BTEMP0150_mK_POW0.0_dB_ATT0.0_dB_in_in.fits"
    file_path = directory + "\\" + filename

    # Create instance of fitsVNAS21Sweep class:
    VNASweep = fitsVNAS21Sweep(file_path=file_path)

    plt.figure(figsize=(8, 6))
    plt.plot(VNASweep.frequency_array * 1e-9, VNASweep.s21_magnitude_db_array)
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.show()


if __name__ == "__main__":
    main()
